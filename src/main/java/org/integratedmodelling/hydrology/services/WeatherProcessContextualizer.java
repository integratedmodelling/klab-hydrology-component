/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.hydrology.weather.Weather;
import org.integratedmodelling.hydrology.weather.WeatherFactory;

/**
 * Create the weather in a scale; store it in the session for quicker future reference.
 * Runs remotely for ARIES/ISU users.
 * 
 * @author Ferd
 *
 */
@Prototype(
        id = "weather.get-weather",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER },
        published = true,
        args = {
                "? yb|years-back",
                Prototype.INT,
                "? a|adjust-data",
                Prototype.BOOLEAN,
                "? nd|no-data-percentage",
                Prototype.INT,
                "? s|create-stations",
                Prototype.BOOLEAN,
                "? cru|force-cru",
                Prototype.BOOLEAN
        },
        argDescriptions = {
                "maximum years in the past accepted to fill in gap years in stations (default 15)",
                "adjust daily data around weather stations based on long-term records in input",
                "maximum percentage of no-data values for a station in a year (0-100; default 100)",
                "create weather stations as subjects at initialization (default false)",
                "force use of monthly CRU data over NOAA weather stations (default false)"})
public class WeatherProcessContextualizer implements IProcessContextualizer {

    boolean                                   dispose           = false;
    IMonitor                                  monitor           = null;

    private Map<String, IObservableSemantics> expectedOutputs;
    private IActiveDirectObservation          context;
    private IScale                            scale;
    private IResolutionScope                  scope;

    int                                       maxYearsBack      = 15;
    int                                       maxNodataAccepted = 100;
    boolean                                   createStations    = false;
    boolean                                   adjust            = true;
    boolean                                   forceCRU          = false;
    String                                    serviceUrl;

    Weather                                   weather;

    @Override
    public boolean canDispose() {
        return dispose;
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.monitor = monitor;
        this.scope = resolutionContext;

        Map<String, IObservation> ret = new HashMap<>();

        /*
         * FIXME - make the "other" scale work right (particularly at transitions) and
         * remove.
         */
        this.scale = process.getScale();
        if (!(process.getScale() instanceof Scale)) {
            this.scale = Scale.sanitize(this.scale);
        }

        if (scale.getTime() == null) {
            throw new KlabValidationException("cannot compute weather without a temporal context");
        }

        /*
         * create the weather. It will automatically look into the states passed to see if
         * there is a version of any outputs, to use for distributing point observations
         * in the area of influence.
         */
        this.weather = WeatherFactory
                .getWeather(scale, context.getStates(), maxYearsBack, adjust, forceCRU, monitor);

        if (this.weather == null) {
            throw new KlabException("not enough weather stations in this area");
        }

        for (String s : expectedOutputs.keySet()) {

            if (scope.isRequired(expectedOutputs.get(s))) {
                IState state = context.getState(expectedOutputs.get(s));
                weather.defineState(state, null);
                ret.put(s, state);
            }
        }

        /*
         * save list of expected observables and context
         */
        this.expectedOutputs = expectedOutputs;
        this.context = context;

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        Map<String, IObservation> ret = new HashMap<>();
        dispose = transition.isLast();

        for (String s : expectedOutputs.keySet()) {
            if (scope.isRequired(expectedOutputs.get(s))) {
                IState state = context.getState(expectedOutputs.get(s));
                weather.defineState(state, transition);
                ret.put(s, state);
            }
        }

        return ret;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        if (parameters.containsKey("years-back")) {
            maxYearsBack = ((Number) parameters.get("years-back")).intValue();
        }
        if (parameters.containsKey("no-data-percentage")) {
            maxNodataAccepted = ((Number) parameters.get("no-data-percentage")).intValue();
        }
        if (parameters.containsKey("create-stations")) {
            createStations = (Boolean) parameters.get("create-stations");
        }
        if (parameters.containsKey("adjust-data")) {
            adjust = (Boolean) parameters.get("adjust-data");
        }
        if (parameters.containsKey("force-cru")) {
            forceCRU = (Boolean) parameters.get("force-cru");
        }
    }

}
