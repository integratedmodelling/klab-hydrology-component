/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology.services;

import java.util.HashMap;
import java.util.Map;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.gis.GISOperations;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.hydrology.WatershedOperations;

/**
 * TODO this becomes the instantiator for watershed in a region if data are not available.
 * Will also compute all qualities, so the following watershed resolver can skip
 * resolution.
 * 
 * Should have a parameter to allow partial watershed generation, limit partial generation
 * to the main watershed found (default), or require complete watershed in context.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "im.hydrology.watersheds",
        args = {
                "# tca-threshold",
                Prototype.FLOAT,
                "# minimum-basin-drainage",
                Prototype.FLOAT,
                "# grid",
                Prototype.BOOLEAN,
                "# add-outlet",
                Prototype.BOOLEAN,
                "# add-states",
                Prototype.BOOLEAN,
                "# whole-watersheds",
                Prototype.BOOLEAN },
        returnTypes = { NS.SUBJECT_INSTANTIATOR })
public class WatershedSubjectInstantiator implements ISubjectInstantiator {

    boolean                                   wholeWatersheds = true;
    boolean                                   grid            = false;
    private boolean                           canDispose;
    private IDirectObservation                context;
    private IState                            elevation;
    private double                            tcaThreshold    = 0.005;
    private double                            basinThreshold  = 0.05;
    private IMonitor                          monitor;
    private IResolutionScope                  resolutionContext;
    private Map<String, IObservableSemantics> expectedOutputs;

    public Map<String, IObservation> extractWatersheds(IState elevation)
            throws KlabException {

        Map<String, IObservation> ret = new HashMap<>();
        WatershedOperations ws = new WatershedOperations(MapUtils
                .of("tca-threshold", tcaThreshold, "watershed-count", (wholeWatersheds ? 1
                        : -1), "minimum-basin-drainage", basinThreshold));
        if (grid) {
            ws.createGridsInWatersheds();
        }
        Map<IObservableSemantics, GridCoverage2D> results = ws
                .watershedAnalysis(elevation, context, monitor);

        for (ISubject watershed : ws.getBasins()) {
            ret.put(watershed.getName(), watershed);
        }

        /*
         * see what we want from the analysis as context states.
         */
        for (String s : expectedOutputs.keySet()) {

            if (!resolutionContext.isRequired(expectedOutputs.get(s))) {
                continue;
            }

            for (IObservableSemantics obs : results.keySet()) {
                if (obs.is(expectedOutputs.get(s))) {
                    ret.put(s, GISOperations.coverageToState(results
                            .get(obs), expectedOutputs
                                    .get(s), context.getContext(), s, null));
                    break;
                }
            }
        }

        return ret;
    }

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {

        if (parameters.containsKey("whole-watersheds")) {
            this.wholeWatersheds = Boolean
                    .parseBoolean(parameters.get("whole-watersheds").toString());
        }
        if (parameters.containsKey("grid")) {
            this.grid = Boolean
                    .parseBoolean(parameters.get("grid").toString());
        }
        if (parameters.containsKey("minimum-basin-drainage")) {
            this.basinThreshold = Double
                    .parseDouble(parameters.get("minimum-basin-drainage").toString());
        }
    }

    @Override
    public void initialize(IActiveSubject context, IResolutionScope resolutionContext, IModel model, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.monitor = monitor;
        this.canDispose = !context.getScale().isTemporallyDistributed();
        this.context = context;
        this.elevation = States.findState(context, GeoNS.ELEVATION);
        this.resolutionContext = resolutionContext;
        this.expectedOutputs = expectedOutputs;

        if (elevation == null) {
            throw new KlabInternalErrorException("watershed accessor cannot find elevation data: aborting");
        }

    }

    @Override
    public Map<String, IObservation> createSubjects(IActiveSubject context, ITransition transition, Map<String, IState> inputs)
            throws KlabException {
        return extractWatersheds(elevation);
    }

}
