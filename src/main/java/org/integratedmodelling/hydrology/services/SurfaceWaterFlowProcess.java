/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.hydrology.HydrologyComponent;
import org.integratedmodelling.hydrology.WatershedOperations;
import org.jgrasstools.gears.libs.modules.Direction;

import com.vividsolutions.jts.geom.Point;

@Prototype(
        id = "im.hydrology.surface-water-flow",
        args = {
                "# curve-number-state",
                Prototype.TEXT
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class SurfaceWaterFlowProcess implements IProcessContextualizer {

    IProject                    project;
    boolean                     canDispose          = false;
    private WatershedOperations wshed;
    private IGrid               grid;
    private IState              precipitation;
    private IState              runoffState;
    private IScale              scale;
    private IState              slope;
    // may be null; provided through parameters for now
    private IState              curveNumber;
    private int[]               curveNumbers;
    private int[]               flowDirections;
    private double[]            runoffCache;
    private double[]            consumedCache;
    private String              runoffId;
    private BitSet              stream;
    private List<ISubject>      outlets             = new ArrayList<>();

    /*
     * if we have these inputs at the scale we run on, we don't use curve numbers. We
     * don't put them in dependencies for now - just leave it to models upstream to
     * provide them. Putting them in as optional dependencies would not make a difference
     * and may pull in models that are lower in quality than the curve number method.
     */
    private IState              infiltration        = null;
    private IState              evaporation         = null;
    private IState              runoffVelocityState = null;
    private String              locallyConsumedId;
    private IState              locallyConsumedState;
    private float[]             mannings;
    private String              runoffVelocityId;
    private String              curveNumberStateId  = null;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        this.project = project;
        if (parameters.containsKey("curve-number-state")) {
            this.curveNumberStateId = parameters.get("curve-number-state").toString();
        }
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        canDispose = !context.getScale().isTemporallyDistributed();
        Map<String, IObservation> ret = new HashMap<>();

        this.wshed = (WatershedOperations) context.getMetadata().get(WatershedOperations.METADATA_FIELD);

        /**
         * FIXME this shouldn't happen for a while, but it may when the watershed
         * formation process dependency isn't there or is resolved by non-Thinklab models.
         * At this point all that would be needed is a dependency on elevation and a call
         * to the WS operations, which we leave off for now, so no big deal to fix.
         */
        if (this.wshed == null) {
            throw new KlabUnsupportedOperationException("watershed has not been initialized by recognized models: this is unsupported at the time being");
        }

        /*
         * Add the outlet
         */
        ISubject streamOutlet = wshed.getOutlet();
        if (streamOutlet != null) {
            ret.put(streamOutlet.getName(), streamOutlet);
        }

        /*
         * add the runoff state to all outlets and put them away
         */
        for (ISubject s : ((ISubject) context).getSubjects()) {
            if (s.getObservable().getSemantics().getType().is(GeoNS.STREAM_OUTLET)) {
                outlets.add(s);
            }
        }

        /*
         * if we get here we have a grid.
         */
        this.scale = context.getScale();
        this.grid = scale.getSpace().getGrid();

        /*
         * analyze expected outputs - for now only pick runoff, then later we'll add the
         * various others. TODO should check units etc. - for now assume we are using our
         * own model for this and the units will be consistent with assumptions.
         */
        runoffVelocityId = null;
        IObservableSemantics runoffVelocityObservable = null; // Is that necessary? Do I
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(GeoNS.RUNOFF_VELOCITY)) {
                runoffVelocityId = iname;
                runoffVelocityObservable = expectedOutputs.get(iname);
                for (ISubject s : outlets) {
                    ((IActiveSubject) s)
                            .getState(new ObservableSemantics((ObservableSemantics) runoffVelocityObservable));
                }
            }
        }

        IObservableSemantics runoffObservable = null;
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(GeoNS.RUNOFF_VOLUME)) {
                runoffId = iname;
                runoffObservable = expectedOutputs.get(iname);
                for (ISubject s : outlets) {
                    ((IActiveSubject) s)
                            .getState(new ObservableSemantics((ObservableSemantics) runoffObservable));
                }
            }
        }
        IObservableSemantics locallyConsumedObservable = null;
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(GeoNS.LOCALLY_EXCHANGED_VOLUME)) {
                locallyConsumedId = iname;
                locallyConsumedObservable = expectedOutputs.get(iname);
            }
        }

        /*
         * TODO/FIXME: the findStateWithout below should be substituted with the
         * findStateWith that uses the temporal trait corresponding to our output. Also,
         * temporal traits should be attributed automatically based on scale.
         */
        IState hsg = States.findState(context, GeoNS.HYDROLOGIC_SOIL_GROUP);
        IState landcover = States.findState(context, GeoNS.GLOBCOVER_CLASS);
        IState flowdir = States.findState(context, GeoNS.FLOW_DIRECTION);
        IState strnet = States.findState(context, GeoNS.STREAM_PRESENCE);

        if (this.curveNumberStateId != null) {
            this.curveNumber = ((DirectObservation) context).getStateWithID(curveNumberStateId);
            if (this.curveNumber == null) {
                throw new KlabValidationException("no state with ID " + this.curveNumberStateId
                        + " was supplied in the model");
            }
        }
        this.slope = States.findState(context, GeoNS.SLOPE);
        this.infiltration = States.findStateWithout(context, GeoNS.INFILTRATED_VOLUME, GeoNS.YEARLY_TRAIT);
        this.evaporation = States.findStateWithout(context, GeoNS.EVAPORATED_VOLUME, GeoNS.YEARLY_TRAIT);
        this.precipitation = States.findStateWithout(context, GeoNS.PRECIPITATION_VOLUME, GeoNS.YEARLY_TRAIT);
        this.runoffState = context.getState(runoffObservable);
        if ((this.infiltration == null || this.evaporation == null) && locallyConsumedObservable != null) {
            this.locallyConsumedState = context.getState(locallyConsumedObservable);
        }
        if (runoffVelocityObservable != null) {
            this.runoffVelocityState = context.getState(runoffVelocityObservable);
        }

        ret.put(runoffId, this.runoffState);

        this.flowDirections = new int[grid.getCellCount()];
        this.stream = new BitSet(grid.getCellCount());
        this.mannings = new float[grid.getCellCount()];

        for (int n : scale.getIndex(IScale.Locator.INITIALIZATION)) {
            int spaceOffset = scale.getExtentOffset(scale.getSpace(), n);

            Object lcov = landcover == null ? null : States.get(landcover, n);
            Object hsgt = States.get(hsg, n);
            Object isst = States.get(strnet, n);
            Object fdir = States.get(flowdir, n);

            if (curveNumber == null) {
                if (this.curveNumbers == null) {
                    this.curveNumbers = new int[grid.getCellCount()];
                }
                curveNumbers[spaceOffset] = HydrologyComponent
                        .getCurveNumber((IConcept) lcov, (IConcept) hsgt);
            }
            flowDirections[spaceOffset] = fdir instanceof Number ? ((Number) fdir).intValue() : -1;
            stream.set(spaceOffset, isst instanceof Boolean ? (Boolean) isst : false);
            if (lcov != null) {
                mannings[spaceOffset] = HydrologyComponent.getManning((IConcept) lcov);
            }
        }

        /*
         * initial runoff
         */
        moveWater(null);

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        /*
         * TODO if land cover or hsg have changed and we're using them, recompute curve
         * numbers
         */

        Map<String, IObservation> ret = new HashMap<>();
        canDispose = transition.isLast();
        moveWater(transition);
        ret.put(runoffId, runoffState);
        if (locallyConsumedState != null) {
            ret.put(locallyConsumedId, locallyConsumedState);
        }
        if (runoffVelocityState != null) {
            ret.put(runoffVelocityId, runoffVelocityState);
        }
        return ret;
    }

    public void moveWater(ITransition transition) throws KlabException {

        if (runoffCache == null) {
            runoffCache = new double[grid.getCellCount()];
        } else {
            Arrays.fill(runoffCache, 0);
        }

        // for (int ofs : scale.getIndex(ITransition.INITIALIZATION)) {
        // States.set()
        // }

        int shitCount = 0;
        int nn = 0;
        /**
         * TODO this can be parallelized as long as writing to the states is atomic. Best
         * to use a local atomic collection for storage and write synchronously to the
         * states afterwards, if so.
         */
        for (IGrid.Cell cell : wshed.getStreamHeads()) {

            double runoff = 0;
            while (cell != null) {

                /*
                 * this will only return a singleton index for the foreseeable future, but
                 * it allows extents beyond space/time with no modification.
                 */
                IScale.Index index = scale.getIndex(transition, grid.getLocator(cell.getX(), cell.getY()));
                int spaceOfs = grid.getOffset(cell.getX(), cell.getY());

                for (int ofs : index) {

                    boolean isRiver = stream.get(spaceOfs);
                    double precip = States.toDouble(precipitation.getValue(ofs));
                    double input = runoff + precip;

                    if (Double.isNaN(input)) {
                        continue;
                    }

                    // if (precip > 0) {
                    // System.out.println("ZOZ");
                    // }

                    /*
                     * use infiltration/evaporation if something provides it upstream, or
                     * curve numbers if not.
                     */
                    if (infiltration != null && evaporation != null) {

                        if (isRiver) {
                            runoffCache[spaceOfs] += clamp(input);
                        } else {
                            double infil = States.toDouble(infiltration.getValue(ofs));
                            double evapo = States.toDouble(evaporation.getValue(ofs));
                            runoffCache[spaceOfs] += clamp(input - infil - evapo);
                        }

                    } else {

                        int cnum = (curveNumber != null
                                ? States.getInteger(curveNumber, spaceOfs, 100)
                                : curveNumbers[spaceOfs]);

                        if (input > 0) {

                            runoffCache[spaceOfs] += (isRiver ? clamp(input)
                                    : HydrologyComponent.getRunoffAmount(clamp(input), cnum));
                            /*
                             * FIXME! if I set the storage directly, this gets flipped
                             * along the X axis.
                             */
                            if (locallyConsumedState != null) {
                                if (consumedCache == null) {
                                    consumedCache = new double[grid.getCellCount()];
                                }
                                consumedCache[spaceOfs] += input - runoff;
                            }
                        }
                    }
                }

                /*
                 * compute runoff to next cell downwards and move there unless the cell
                 * ends up draining outside the basin.
                 */
                int fdir = flowDirections[spaceOfs];
                if (fdir < 0) {
                    cell = null;
                    shitCount++;
                } else {
                    Direction direction = Direction.forFlow(flowDirections[spaceOfs]);
                    cell = cell.move(direction.col, direction.row);
                }
            }
        }

        /*
         * set cumulated runoff values into state
         */
        for (int n : scale.getIndex(transition)) {
            int spaceOfs = scale.getExtentOffset(scale.getSpace(), n);
            States.set(runoffState, runoffCache[spaceOfs], n);
            if (consumedCache != null) {
                States.set(locallyConsumedState, consumedCache[spaceOfs], n);
            }

            if (runoffVelocityState != null) {
                double slp = States.toDouble(slope.getValue(spaceOfs));
                double runoffVelocity = HydrologyComponent
                        .getRunoffVelocity(runoffCache[spaceOfs], mannings[spaceOfs], ((Grid) grid)
                                .getCellWidthMeters(), slp);
                States.set(runoffVelocityState, runoffVelocity, n);
            }
        }

        /*
         * update runoff states for outlets
         */
        for (ISubject s : outlets) {

            Point location = ((IGeometricShape) s.getScale().getSpace().getShape()).getGeometry()
                    .getCentroid();
            int spaceofs = grid.getOffsetFromWorldCoordinates(location.getX(), location.getY());

            for (IState st : s.getStates()) {
                if (st.getObservable().getSemantics().is(GeoNS.RUNOFF_VOLUME)) {
                    States.set(st, runoffCache[spaceofs], transition == null ? 0 : transition.getTimeIndex());
                } else if (st.getObservable().getSemantics().is(GeoNS.RUNOFF_VELOCITY)) {
                    double slp = States.toDouble(slope.getValue(spaceofs));
                    States.set(st, HydrologyComponent
                            .getRunoffVelocity(runoffCache[spaceofs], mannings[spaceofs], ((Grid) grid)
                                    .getCellWidthMeters(), slp), transition == null ? 0
                                            : transition.getTimeIndex());
                }

            }
        }

    }

    private double clamp(double d) {
        return d < 0 ? 0 : d;
    }
}
