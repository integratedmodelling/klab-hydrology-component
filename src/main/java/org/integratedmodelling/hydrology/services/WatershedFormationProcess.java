/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology.services;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.gis.GISOperations;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.hydrology.WatershedOperations;

@Prototype(id = "im.hydrology.watershed-formation", args = { "# tca-threshold", Prototype.FLOAT, "# watershed-count",
		Prototype.INT }, returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class WatershedFormationProcess implements IProcessContextualizer {

	IProject project;
	boolean canDispose = false;
	Map<String, Object> parameters = null;

	@Override
	public boolean canDispose() {
		return canDispose;
	}

	@Override
	public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
		this.project = project;
		this.parameters = parameters;
	}

	@Override
	public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context,
			IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs,
			Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor) throws KlabException {

		Map<String, IObservation> ret = new HashMap<>();

		canDispose = !context.getScale().isTemporallyDistributed();

		IState elevation = States.findState(context, GeoNS.ELEVATION);
		if (elevation == null) {
			throw new KlabInternalErrorException("watershed accessor cannot find elevation data: aborting");
		}

		/*
		 * cross fingers
		 */
		WatershedOperations ws = new WatershedOperations(parameters);
		Map<IObservableSemantics, GridCoverage2D> results = ws.watershedAnalysis(elevation, context, monitor);

		for (String s : expectedOutputs.keySet()) {

			if (!resolutionContext.isRequired(expectedOutputs.get(s))) {
				continue;
			}

			for (IObservableSemantics obs : results.keySet()) {
				if (obs.is(expectedOutputs.get(s))) {

					Function<Double, Double> transform = null;

					if (obs.is(GeoNS.TOTAL_CONTRIBUTING_AREA)) {
						double cellSize = ((Grid)context.getScale().getSpace().getGrid()).getCellAreaMeters();
						transform = d -> d * cellSize;
					}
					
					ret.put(s, GISOperations.coverageToState(results.get(obs), expectedOutputs.get(s), context, s, transform));
					break;
				}
			}
		}

		return ret;
	}

	@Override
	public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs) throws KlabException {
		// TODO if anything non-final has changed, we want to recompute and turn
		// the states into
		// dynamic ones.
		// Env.logger.info("WATERSHED FORMATION EXECUTING " +
		// transition.getTimeIndex());
		canDispose = transition.isLast();
		return null;
	}

}
