/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.components.Component;
import org.integratedmodelling.api.components.Initialize;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.engine.geospace.extents.Grid;

@Component(id = "im.hydrology", version = Version.CURRENT, worldview = "im")
public class HydrologyComponent {

	/*
	 * holds mappings between a concept name (just local ID) and the
	 * corresponding index in the CN table below.
	 */
	private static Map<String, Integer> globCoverIndex = new HashMap<>();
	private static Map<String, Integer> hsgIndex = new HashMap<>();
	private static Map<String, Float> manningnIndex = new HashMap<>(); // In
																		// relation
																		// to
																		// water
																		// velocity
																		// and
																		// based
																		// on la

	/*
	 * index with hsgIndex, globCoverIndex after looking up both from the
	 * im.landcover.lccs and im.hydrology concept IDs in the maps above.
	 */
	private static int[][] curveNumberTable = { 
	        { 73, 71, 64, 63, 51, 60, 48, 44, 55, 55, 100, 100, 100, 75 },
			{ 82, 80, 74, 73, 68, 76, 62, 65, 66, 66, 100, 100, 100, 80 },
			{ 88, 86, 81, 82, 78, 81, 73, 77, 74, 74, 100, 100, 100, 85 },
			{ 90, 86, 84, 87, 82, 89, 78, 82, 79, 79, 100, 100, 100, 90 } 
	};

	@Initialize
	public boolean initialize() {
		try {

			GeoNS.synchronize();

			/*
			 * initialize lookup tables for CN calculation
			 */

			hsgIndex.put("SoilGroupA", 0);
			hsgIndex.put("SoilGroupB", 1);
			hsgIndex.put("SoilGroupC", 2);
			hsgIndex.put("SoilGroupD", 3);

			/*
			 * best guess to match Table 1 in Mutua et al. S. Willcock, P.C.
			 */
			globCoverIndex.put("IrrigatedOrFloodedCropland", 1);
			globCoverIndex.put("RainfedCropland", 2);
			globCoverIndex.put("MosaicCropland", 3);
			globCoverIndex.put("MosaicVegetation", 7);
			globCoverIndex.put("ClosedEvergreenOrSemiDeciduousForest", 8);
			globCoverIndex.put("ClosedBroadleavedDeciduousForest", 8);
			globCoverIndex.put("OpenBroadleavedDeciduousForest", 8);
			globCoverIndex.put("ClosedNeedleleavedEvergreenForest", 8);
			globCoverIndex.put("OpenNeedleleavedDeciduousOrEvergreenForest", 9);
			globCoverIndex.put("MixedBroadleavedAndNeedleleavedForest", 9);
			globCoverIndex.put("MosaicForestShrublandWithGrassland", 7);
			globCoverIndex.put("MosaicGrasslandWithForestShrubland", 7);
			globCoverIndex.put("Shrubland", 6);
			globCoverIndex.put("Grassland", 5);
			globCoverIndex.put("SparseVegetation", 13);
			globCoverIndex.put("ClosedFloodedForestFreshwater", 12);
			globCoverIndex.put("ClosedFloodedForestSalineWater", 12);
			globCoverIndex.put("ClosedFloodedVegetation", 11);
			globCoverIndex.put("ArtificialSurfaces", 0);
			globCoverIndex.put("BareAreas", 13);
			globCoverIndex.put("WaterBodies", 10);
			globCoverIndex.put("PermanentSnowAndIce", 10);

			/*
			 * best guess to match table 3.1 (see
			 * https://integratedmodelling.org/confluence/display/ASSETS/
			 * ASSETS_models?preview=/1048629/7209344/Manning_coeffs.png) J.
			 * Martinez-Lopez
			 */
			manningnIndex.put("IrrigatedOrFloodedCropland", 0.06f);
			manningnIndex.put("RainfedCropland", 0.06f);
			manningnIndex.put("MosaicCropland", 0.06f);
			manningnIndex.put("MosaicVegetation", 0.24f);
			manningnIndex.put("ClosedEvergreenOrSemiDeciduousForest", 0.8f);
			manningnIndex.put("ClosedBroadleavedDeciduousForest", 0.8f);
			manningnIndex.put("OpenBroadleavedDeciduousForest", 0.4f);
			manningnIndex.put("ClosedNeedleleavedEvergreenForest", 0.8f);
			manningnIndex.put("OpenNeedleleavedDeciduousOrEvergreenForest", 0.4f);
			manningnIndex.put("MixedBroadleavedAndNeedleleavedForest", 0.4f);
			manningnIndex.put("MosaicForestShrublandWithGrassland", 0.3f);
			manningnIndex.put("MosaicGrasslandWithForestShrubland", 0.3f);
			manningnIndex.put("Shrubland", 0.4f);
			manningnIndex.put("Grassland", 0.24f);
			manningnIndex.put("SparseVegetation", 0.15f);
			manningnIndex.put("ClosedFloodedForestFreshwater", 0.05f);
			manningnIndex.put("ClosedFloodedForestSalineWater", 0.05f);
			manningnIndex.put("ClosedFloodedVegetation", 0.05f);
			manningnIndex.put("ArtificialSurfaces", 0.011f);
			manningnIndex.put("BareAreas", 0.011f);
			manningnIndex.put("WaterBodies", 0.029f); // treat as stream
														// network? the presence
														// of stream network
														// channels will be
														// assessed differently
														// (based on the
														// watershed modelling
														// outputs)
			manningnIndex.put("PermanentSnowAndIce", 0.011f);

		} catch (Throwable e) {
			return false;
		}
		return true;
	}

	/**
	 * Return the curve number for the given landcover/soil group combination
	 * according to Mutua et al. Assume that if no match is found with known
	 * classes, the runoff is 100, i.e. total.
	 * 
	 * @param landcover
	 * @param hsg
	 * @return curve number
	 */
	public static int getCurveNumber(IConcept landcover, IConcept hsg) {

		if (landcover == null || hsg == null) {
			return 100;
		}

		Integer h = hsgIndex.get(hsg.getLocalName());
		Integer l = globCoverIndex.get(landcover.getLocalName());

		if (h != null && l != null) {
			return curveNumberTable[h][l];
		}

		return 100;
	}

	/**
	 * Return the Manning's coefficient for the given landcover according to
	 * https://integratedmodelling.org/confluence/display/ASSETS/ASSETS_models
	 * Assume that if no match is found with known classes, the Manning's
	 * coefficient is 0.4
	 * 
	 * @param landcover
	 * @return
	 */
	public static float getManning(IConcept landcover) {

		if (landcover == null) {
			return 0.4f; // LC with average value?
		}

		// Double mn = manningnIndex.get(landcover.getLocalName());

		// return mn; // Am I right?

		return manningnIndex.get(landcover.getLocalName());
	}

	/**
	 * Get the amount of the passed precipitation that runs off a cell with the
	 * passed types.
	 * 
	 * @param precipitation
	 * @param landcover
	 * @param hsg
	 * @return runoff amount
	 */
	public static double getRunoffAmount(double precipitation, IConcept landcover, IConcept hsg) {
		return getRunoffAmount(precipitation, getCurveNumber(landcover, hsg));
	}

	/**
	 * Get the amount of the passed precipitation that runs off a cell with this
	 * curve number.
	 * 
	 * @param precipitation
	 * @param curveNumber
	 * @return runoff amount
	 */
	public static double getRunoffAmount(double precipitation, int curveNumber) {

//	    return precipitation * (curveNumber/100.0);
	    
		double retention = 254.0 * ((100.0 / curveNumber) - 1);
		if (precipitation > (0.2 * retention)) {
			return ((precipitation - (0.2 * retention)) * (precipitation - (0.2 * retention)))
					/ (precipitation + (0.8 * retention));
		}
		return 0;
	}

	/**
	 * Get the amount of time needed for the run-off to cross the cell. UNDER
	 * DEVELOPMENT!
	 * 
	 * @param gridsize
	 *            Length of the cell
	 * @param mn
	 *            Manning's coefficient of the cell
	 * @param runoffCache
	 *            Runoff amount passed to the object (after the final/total
	 *            runoff has been computed!)
	 * @param slope
	 *            Slope of the cell
	 * @return
	 */
	public static double getRunoffVelocity(IConcept landcover, IGrid grid, double runoffCache, double slope) { // IConcept
																									// slope,
																									// double
																									// gridsize,//
																									// ((Grid)grid).getCellWidthMeters()
																									// here
																									// or
																									// in
																									// surfacewaterflowprocess_velocity?
		return getRunoffVelocity(((Grid)grid).getCellWidthMeters(), getManning(landcover), runoffCache, slope); //
	}

	/*
	 * private static double getRunoffVelocity(double precipitation, double mn,
	 * IState slope, double gridSize) { // TODO Auto-generated method stub
	 * return 0; }
	 */
	/**
	 * Get the amount of time needed for the run-off to cross the cell. UNDER
	 * DEVELOPMENT!
	 * 
	 * @param gridsize
	 *            Length of the cell
	 * @param mn
	 *            Manning's coefficient of the cell
	 * @param runoffCache
	 *            Runoff amount passed to the object (after the final/total
	 *            runoff has been computed!)
	 * @param slope
	 *            Slope of the cell
	 * @return
	 */
	public static double getRunoffVelocity(double runoffCache, double manning, double gridsize, double slope) {

		if (runoffCache == 0) {
			return 0; // to avoid infinite value
		} else {
			return gridsize * 1440 / (Math.pow(gridsize * 3.28084 * manning, 0.6) * 0.94
					/ (Math.pow(runoffCache * 0.03937008 / 24, 0.4) * Math.pow(Math.tan(slope), 0.3)));
		}
	}

}
