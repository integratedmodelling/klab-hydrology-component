/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology;

import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.contextualizers.FeatureExtractor;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.gis.GISOperations;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.jgrasstools.gears.libs.modules.FlowNode;
import org.jgrasstools.gears.libs.modules.Variables;
import org.jgrasstools.gears.utils.RegionMap;
import org.jgrasstools.gears.utils.coverage.CoverageUtilities;
import org.jgrasstools.hortonmachine.modules.demmanipulation.markoutlets.OmsMarkoutlets;
import org.jgrasstools.hortonmachine.modules.demmanipulation.pitfiller.OmsPitfiller;
import org.jgrasstools.hortonmachine.modules.demmanipulation.wateroutlet.OmsExtractBasin;
import org.jgrasstools.hortonmachine.modules.geomorphology.flow.OmsFlowDirections;
import org.jgrasstools.hortonmachine.modules.geomorphology.slope.OmsSlope;
import org.jgrasstools.hortonmachine.modules.geomorphology.tca.OmsTca;
import org.jgrasstools.hortonmachine.modules.network.extractnetwork.OmsExtractNetwork;

import com.vividsolutions.jts.geom.Point;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.hydrology.fillSinks.FillSinksAlgorithm;
import es.unex.sextante.outputs.Output;

public class WatershedOperations extends GISOperations {

    class OutletData {
        double         x;
        double         y;
        double         importance;
        ISubject       predefined;

        /**
         * Spatial extent with grid for corresponding watershed.
         */
        ISpatialExtent space;

        OutletData(double x, double y, double importance, IDirectObservation s) {
            this.x = x;
            this.y = y;
            this.importance = importance;
            this.predefined = (ISubject) s;
        }
    }

    private boolean               createGridInInstances      = false;
    Collection<IGrid.Cell>        streamHeads                = null;
    private GridCoverage2D        pitFilledElevation         = null;
    private GridCoverage2D        flowDirections             = null;
    private GridCoverage2D        drainageArea               = null;
    private ArrayList<OutletData> outlets                    = new ArrayList<>();
    private GridCoverage2D        streamNetwork              = null;
    private IDirectObservation    context;
    ISubject                      predefinedOutlet           = null;

    /*
     * these only get instantiated when getBasins() is called and serve as backing states
     * for the corresponding states in each basin extracted.
     */
    private IState                flowDirectionsState        = null;
    private IState                drainageAreaState          = null;
    private IState                streamNetworkState         = null;

    public final static String    INFO_CLASS                 = "GIS_PROCESS";
    private double                BASIN_IMPORTANCE_THRESHOLD = 0.05;
    private int                   WATERSHEDS_TO_EXTRACT      = 1;
    public static final String    METADATA_FIELD             = "WATERSHED_OPS";
    private double                TCA_THRESHOLD              = 0.09;
    private Grid                  grid;
    private TaskMonitor           taskMonitor;
    private IState                dem;

    /*
     * these can be set when extracting basins
     */
    private boolean               addStates                  = false;
    private boolean               addOutlet                  = false;

    /**
     * Admits a map of parameters.
     * 
     * @param parameters
     */
    public WatershedOperations(Map<String, Object> parameters) {
        if (parameters != null) {
            for (String s : parameters.keySet()) {
                if (s.equals("tca-threshold")) {
                    TCA_THRESHOLD = ((Number) parameters.get(s)).doubleValue();
                } else if (s.equals("watershed-count")) {
                    WATERSHEDS_TO_EXTRACT = ((Number) parameters.get(s)).intValue();
                } else if (s.equals("add-outlet")) {
                    addOutlet = ((Boolean) parameters.get(s));
                } else if (s.equals("add-states")) {
                    addStates = ((Boolean) parameters.get(s));
                } else if (s.equals("minimum-basin-drainage")) {
                    BASIN_IMPORTANCE_THRESHOLD = ((Number) parameters.get(s)).doubleValue();
                }
            }
        }
    }

    public Collection<IGrid.Cell> getStreamHeads() throws KlabException {

        if (streamHeads != null) {
            return streamHeads;
        }

        List<IGrid.Cell> ret = new ArrayList<>();
        RegionMap regionMap = CoverageUtilities
                .getRegionParamsFromGridCoverage(flowDirections);
        int cols = regionMap.getCols();
        int rows = regionMap.getRows();

        RenderedImage flowRI = flowDirections.getRenderedImage();
        RandomIter flowIter = RandomIterFactory.create(flowRI, null);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int y = grid.getYCells() - r - 1;
                // WHAT?
                if (grid.getActivationLayer().isActive(c, y)) {
                    FlowNode flowNode = new FlowNode(flowIter, cols, rows, c, r);
                    if (flowNode.isSource()) {
                        ret.add(grid.getCell(c, y));
                    }
                }
            }
        }
        flowIter.done();

        streamHeads = ret;

        return ret;
    }

    /**
     * Simply pass a non-corrected DEM to receive the same DEM with pits filled. Won't do
     * any checking on the input semantics. Of course it will complain vigorously if not a
     * spatial raster state.
     * 
     * TODO switch to JGrassTools.
     * 
     * @param demState
     * @param context
     * @param monitor
     * @return state with filled sinks
     * @throws KlabException
     */
    public static IState fillSinks(IState demState, ISubject context, IMonitor monitor)
            throws KlabException {

        FillSinksAlgorithm alg = new FillSinksAlgorithm();
        ParametersSet parms = alg.getParameters();
        Output fdem = null;
        TaskMonitor mon = new TaskMonitor(monitor);

        IRasterLayer dem = stateToRaster(demState);

        try {
            parms.getParameter(FillSinksAlgorithm.DEM).setParameterValue(dem);
            OutputFactory outputFactory = new GTOutputFactory();

            alg.execute(mon, outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            fdem = outputs.getOutput(FillSinksAlgorithm.RESULT);

        } catch (Exception e) {
            monitor.error(e);
            throw new KlabValidationException(e);
        }
        return rasterToState((IRasterLayer) fdem.getOutputObject(), demState
                .getObservable().getSemantics(), context);
    }

    /**
     * Perform the classic set of watershed computations and return all states produced.
     * This one uses JGrass.
     * 
     * @param demState
     * @param context
     * @param monitor
     * 
     * @return all datasets
     * @throws KlabException
     */
    public Map<IObservableSemantics, GridCoverage2D> watershedAnalysis(IState demState, IDirectObservation context, IMonitor monitor)
            throws KlabException {

        this.context = context;
        this.context.getMetadata().put(METADATA_FIELD, this);
        this.taskMonitor = new TaskMonitor(monitor);
        this.dem = demState;

        Map<IObservableSemantics, GridCoverage2D> ret = new HashMap<>();
        this.grid = ((SpaceExtent) (demState.getSpace())).getGrid();
        GridCoverage2D dem = stateToCoverage(demState);

        // System.out.println("DEM: " + GISOperations.countNodata(dem,
        // context));

        /*
         * Pit filling.
         * 
         * TODO intercept pit estimate and provide feedback - this one is the longest
         * running. Also there should be a provision for caching.
         */
        OmsPitfiller pitfiller = new OmsPitfiller();
        pitfiller.inElev = dem;
        pitfiller.pm = this.taskMonitor;
        pitfiller.doProcess = true;
        pitfiller.doReset = false;
        monitor.info("filling hydrological sinks...", Messages.INFOCLASS_MODEL);
        try {
            pitfiller.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        this.pitFilledElevation = pitfiller.outPit;

        // System.out.println("PIT: " +
        // GISOperations.countNodata(pitFilledElevation, context));

        /*
         * TODO these must also contain the observer
         */
        ret.put(new ObservableSemantics(GeoNS.PIT_FILLED_ELEVATION, KLAB
                .c(NS.MEASUREMENT), "pit-filled-elevation"), pitfiller.outPit);

        /*
         * flow directions
         */
        OmsFlowDirections omsflowdirections = new OmsFlowDirections();
        omsflowdirections.inPit = pitfiller.outPit;
        omsflowdirections.pm = pitfiller.pm;
        omsflowdirections.doProcess = true;
        omsflowdirections.doReset = false;
        monitor.info("computing flow directions...", INFO_CLASS);
        try {
            omsflowdirections.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        this.flowDirections = omsflowdirections.outFlow;

        // System.out.println("FLOW: " +
        // GISOperations.countNodata(flowDirections, context));

        ret.put(new ObservableSemantics(GeoNS.FLOW_DIRECTION, KLAB
                .c(NS.RANKING), "flow-direction"), omsflowdirections.outFlow);

        /*
         * omstca!
         */
        OmsTca omstca = new OmsTca();
        omstca.inFlow = omsflowdirections.outFlow;
        omstca.doProcess = true;
        omstca.doReset = false;
        omstca.pm = omsflowdirections.pm;
        monitor.info("computing drainage area per cell...", INFO_CLASS);
        try {
            omstca.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        this.drainageArea = omstca.outTca;

        /*
         * TODO intercept warnings from watershed delineation that doesn't end at the
         * boundaries and push them up to the monitor.
         */
        ret.put(new ObservableSemantics(GeoNS.TOTAL_CONTRIBUTING_AREA, KLAB
                .c(NS.RANKING), "total-contributing-area"), omstca.outTca);

        /*
         * outlets
         */
        OmsMarkoutlets out = new OmsMarkoutlets();
        out.inFlow = omsflowdirections.outFlow;
        out.doProcess = true;
        out.doReset = false;
        out.pm = omsflowdirections.pm;
        monitor.info("finding outlets...", INFO_CLASS);
        try {
            out.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        /*
         * extract all outlets as single coordinates first, then make objects out of them.
         */
        RenderedImage image = out.outFlow.getRenderedImage();
        int npix = image.getHeight() * image.getWidth();
        RandomIter itera = RandomIterFactory.create(image, null);
        RenderedImage imtca = omstca.outTca.getRenderedImage();
        RandomIter itca = RandomIterFactory.create(imtca, null);

        List<IDirectObservation> outs = findExistingOutlets(context);

        if (outs.size() > 0) {

            for (IDirectObservation o : outs) {
                Point pt = ((IGeometricShape) o.getScale().getSpace().getShape())
                        .getGeometry().getCentroid();
                int[] xy = grid.getGridCoordinatesAt(pt.getX(), pt.getY());
                double importance = (itca.getSampleDouble(xy[0], xy[1], 0) / (npix));
                this.outlets.add(new OutletData(pt.getX(), pt.getY(), importance, o));
            }

            monitor.info("using " + outs.size() + " predefined outlet"
                    + (outs.size() == 1 ? "" : "s"), INFO_CLASS);

        } else {

            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    if (itera.getSampleDouble(x, y, 0) == FlowNode.OUTLET) {
                        double importance = (itca.getSampleDouble(x, y, 0) / (npix));
                        if (importance >= BASIN_IMPORTANCE_THRESHOLD) {
                            monitor.debug("outlet at " + x + "," + y + " drains "
                                    + (importance * 100) + "% of context");
                            double[] xy = grid.getCoordinatesAt(x, y);
                            this.outlets
                                    .add(new OutletData(xy[0], xy[1], importance, null));
                        }
                    }
                }
            }

            monitor.info("found " + outlets.size() + " basins covering "
                    + StringUtils.percent(BASIN_IMPORTANCE_THRESHOLD)
                    + " or more", INFO_CLASS);

        }

        int activeCells = 0;

        if (outlets.size() > 0) {

            /*
             * sort outlets in descending drainage area order and take the one(s) with
             * highest drainage.
             */
            Collections.sort(outlets, new Comparator<OutletData>() {

                @Override
                public int compare(OutletData arg0, OutletData arg1) {
                    return new Double(arg1.importance).compareTo(arg0.importance);
                }
            });

            if (outs.size() > 0) {
                predefinedOutlet = outlets.get(0).predefined;
            }

            for (int i = 0; (WATERSHEDS_TO_EXTRACT <= 0 ? true
                    : i < WATERSHEDS_TO_EXTRACT) && i < outlets.size(); i++) {

                monitor.info("basin " + (i + 1) + " covers "
                        + StringUtils.percent(outlets.get(i).importance)
                        + " of area", INFO_CLASS);

                /*
                 * make a new mask for the main grid
                 */
                IGridMask mask = new RasterActivationLayer(grid.getXCells(), grid
                        .getYCells(), true, grid);

                /*
                 * extract the basin. TODO make this and the threshold configurable.
                 */
                OmsExtractBasin ebasin = new OmsExtractBasin();
                ebasin.inFlow = omsflowdirections.outFlow;
                ebasin.inFlow = omsflowdirections.outFlow;
                ebasin.pEast = outlets.get(i).x;
                ebasin.pNorth = outlets.get(i).y;
                ebasin.doProcess = true;
                ebasin.doReset = false;

                /*
                 * FIXME (actually FIXIT) - JAI can't find the Vectorize operation called
                 * from inside jgrasstools, so attempts to create a vector shape from
                 * inside the algorithm fail with an IllegalOperationException. Fall back
                 * to (wasteful) FeatureExtractor approach for now.
                 */
                ebasin.doVector = false;
                try {
                    ebasin.process();
                } catch (Exception e) {
                    throw new KlabException(e);
                }

                /*
                 * load watershed mask into scale's activation layer, merging with any
                 * current mask already set.
                 */
                RenderedImage bimg = ebasin.outBasin.getRenderedImage();
                RandomIter bit = RandomIterFactory.create(bimg, null);
                for (int x = 0; x < image.getWidth(); x++) {
                    for (int y = 0; y < image.getHeight(); y++) {
                        if (bit.getSampleDouble(x, y, 0) == 1.0) {
                            if (mask.isActive(x, y)) {
                                mask.activate(x, y);
                            }
                        } else {
                            mask.deactivate(x, y);
                        }
                    }
                }

                activeCells += mask.totalActiveCells();

                /**
                 * Extract shape of watershed using the mask - jgrasstools will throw an
                 * exception for internal reasons.
                 */
                Collection<ShapeValue> shapes = new FeatureExtractor(context
                        .getScale(), monitor, false, true, false).extractShapes(mask);
                if (shapes.size() > 0) {

                    ShapeValue shape = shapes.size() == 1 ? shapes.iterator().next()
                            : /*
                               * TODO convex hull
                               */ null;
                    if (shape != null) {

                        ISpatialExtent space = null;
                        if (createGridInInstances) {
                            IGrid newgrid = ((Grid) context.getScale().getSpace().getGrid())
                                    .cutToShape(shape);
                            space = new SpaceExtent(shape, newgrid);
                        } else {
                            space = shape.asExtent();
                        }

                        outlets.get(i).space = space;
                    }
                }
            }

        } else {
            monitor.warn("no watersheds found: check DEM");
        }

        /*
         * slope for the channel network
         */
        OmsSlope slope = new OmsSlope();
        slope.inFlow = omsflowdirections.outFlow;
        slope.inPit = pitfiller.outPit;
        slope.doProcess = true;
        slope.doReset = false;
        slope.pm = pitfiller.pm;
        try {
            slope.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        monitor.info("extracting channel network...", INFO_CLASS);

        /*
         * channel network TODO try to observe a stream layer and use it to anchor the
         * streams if available (it's another jgrass module - see HM presentation).
         */
        OmsExtractNetwork onet = new OmsExtractNetwork();
        onet.inFlow = omsflowdirections.outFlow;
        onet.inTca = omstca.outTca;
        // onet.inSlope = slope.outSlope;

        // TODO pass area threshold from parameters and in future, scale
        // analysis. For
        // now, take the cells
        // that drain
        // at least 9% of the basin area.
        onet.pThres = activeCells * TCA_THRESHOLD;
        onet.pMode = Variables.TCA;

        monitor.info("TCA threshold is " + onet.pThres, INFO_CLASS);

        onet.pm = pitfiller.pm;
        onet.doProcess = true;
        onet.doReset = false;
        try {
            onet.process();
        } catch (Exception e) {
            throw new KlabException(e);
        }

        monitor.info("watershed analysis completed.", INFO_CLASS);

        this.streamNetwork = onet.outNet;

        ret.put(new ObservableSemantics(GeoNS.STREAM_PRESENCE, KLAB
                .c(NS.PRESENCE_OBSERVATION), "stream-presence"), onet.outNet);

        if (KLAB.CONFIG.isDebug()) {
            for (GridCoverage2D g : ret.values()) {
                g.show();
            }
        }

        return ret;
    }

    private List<IDirectObservation> findExistingOutlets(IDirectObservation context2) {

        List<IDirectObservation> ret = new ArrayList<>();
        for (ISubject s : ((ISubject) context2).getSubjects()) {
            if (s.getObservable().getSemantics().is(GeoNS.STREAM_OUTLET)) {
                ret.add(s);
            }
        }
        return ret;
    }

    /**
     * After initialization, create the main outlet subject in the context and return it.
     * 
     * @return the outlet for the main basin
     * @throws KlabException
     */
    public ISubject getOutlet() throws KlabException {

        if (predefinedOutlet != null) {
            return predefinedOutlet;
        }

        if (this.context == null || outlets.size() < 1) {
            return null;
        }

        List<IExtent> exts = new ArrayList<>();
        for (IExtent e : context.getScale()) {
            if (e instanceof ISpatialExtent) {
                exts.add(new ShapeValue(outlets.get(0).x, outlets.get(0).y, Geospace.get()
                        .getDefaultCRS()).asExtent());
            } else {
                exts.add(e);
            }
        }
        IScale scale = new Scale(exts.toArray(new IExtent[exts.size()]));

        /*
         * TODO use a flows-into relationship so we can account for it automatically.
         */
        if (context instanceof ISubject) {
            return ((IActiveSubject) context)
                    .newSubject(new ObservableSemantics(GeoNS.STREAM_OUTLET), scale, context
                            .getName()
                            + "-stream-outlet", KLAB.p(NS.CONTAINS_PART_PROPERTY));
        }

        return null;

    }

    /**
     * Like getOutlet() but with the option of creating independent subjects that can be
     * added later to the subject of choice.
     * 
     * @param addToContext
     * @return
     * @throws KlabException
     */
    public List<Pair<ISubject, Double>> getOutlets(boolean addToContext)
            throws KlabException {
        List<Pair<ISubject, Double>> ret = new ArrayList<>();

        if (this.context == null || outlets.size() < 1) {
            return null;
        }

        for (OutletData outlet : outlets) {

            if (outlet.predefined != null) {
                ret.add(new Pair<>(outlet.predefined, outlet.importance));
                continue;
            }

            List<IExtent> exts = new ArrayList<>();
            for (IExtent e : context.getScale()) {
                if (e instanceof ISpatialExtent) {
                    exts.add(new ShapeValue(outlet.x, outlet.y, Geospace.get()
                            .getDefaultCRS()).asExtent());
                } else {
                    exts.add(e);
                }
            }
            IScale scale = new Scale(exts.toArray(new IExtent[exts.size()]));

            /*
             * TODO use a flows-into relationship so we can account for it automatically.
             */
            if (context instanceof ISubject) {
                if (addToContext) {

                    ret.add(new Pair<>(((IActiveSubject) context)
                            .newSubject(new ObservableSemantics(GeoNS.STREAM_OUTLET), scale, context
                                    .getName() + "-stream-outlet", KLAB
                                            .p(NS.CONTAINS_PART_PROPERTY)), outlet.importance));
                } else {

                    ret.add(new Pair<>(((Subject) context)
                            .newSubject(new ObservableSemantics(GeoNS.STREAM_OUTLET), scale, context
                                    .getName() + "-stream-outlet", KLAB
                                            .p(NS.CONTAINS_PART_PROPERTY), false), outlet.importance));
                }
            }
        }
        return ret;
    }

    /**
     * After initialization, create all outlet subjects in the context and return them
     * along with the basin area each of them drains.
     * 
     * @return all outlets
     * @throws KlabException
     */
    public List<Pair<ISubject, Double>> getOutlets() throws KlabException {
        return getOutlets(true);
    }

    public List<ISubject> getBasins() throws KlabException {

        List<ISubject> ret = new ArrayList<>();

        if (taskMonitor == null) {
            throw new KlabInternalErrorException("getBasin() must be called after watershedAnalysis()");
        }

        /*
         * FIXME setting these should be a one-line op.
         */
        if (drainageAreaState == null && drainageArea != null) {
            ObservableSemantics obs = new ObservableSemantics(GeoNS.TOTAL_CONTRIBUTING_AREA, KLAB
                    .c(NS.RANKING), "total-contributing-area");
            obs.setObserver(ModelFactory.rankObserver(obs.getType(), null));
            drainageAreaState = GISOperations
                    .coverageToState(drainageArea, obs, context
                            .getContext(), "total-contributing-area", null);
            ((State) drainageAreaState).setParent(context);
        }
        if (streamNetworkState == null && streamNetwork != null) {
            ObservableSemantics obs = new ObservableSemantics(GeoNS.STREAM_PRESENCE, KLAB
                    .c(NS.PRESENCE_OBSERVATION), "stream-presence");
            obs.setObserver(ModelFactory.presenceObserver(obs));
            streamNetworkState = GISOperations
                    .coverageToState(streamNetwork, obs, context
                            .getContext(), "stream-presence", null);
            ((State) streamNetworkState).setParent(context);
        }
        if (flowDirectionsState == null && flowDirections != null) {
            ObservableSemantics obs = new ObservableSemantics(GeoNS.FLOW_DIRECTION, KLAB
                    .c(NS.RANKING), "flow-direction");
            obs.setObserver(ModelFactory.rankObserver(obs.getType(), null));
            flowDirectionsState = GISOperations
                    .coverageToState(flowDirections, obs, context
                            .getContext(), "flow-direction", null);
            ((State) flowDirectionsState).setParent(context);
        }

        List<Pair<ISubject, Double>> outs = getOutlets(false);
        for (int i = 0; i < outs.size(); i++) {

            /*
             * create shape and scale from it.
             */
            if (outlets.get(i).space == null) {
                continue;
            }

            List<IExtent> exts = new ArrayList<>();
            for (IExtent e : context.getScale()) {
                if (e instanceof ISpatialExtent) {
                    /*
                     * exts.add(new grid with given resolution and computed shape)
                     */
                    exts.add(outlets.get(i).space);
                } else {
                    exts.add(e);
                }
            }

            /*
             * make watershed with cutout scale. TODO at this point it may be OK to use a
             * specialized subject.
             */
            IScale scale = new Scale(exts.toArray(new IExtent[exts.size()]));
            ISubject watershed = ((IActiveSubject) context)
                    .newSubject(new ObservableSemantics(GeoNS.WATERSHED), scale, context
                            .getName() + "-watershed-"
                            + (i + 1), KLAB.p(NS.CONTAINS_PART_PROPERTY));

            /*
             * add outlet subject
             */
            if (addOutlet) {
                ISubject outlet = outs.get(i).getFirst();
                ((Subject) watershed)
                        .addSubject(outlet, KLAB.p(NS.CONTAINS_PART_PROPERTY));
            }
            /*
             * cutout river network, TCA and flow directions to new extent and add as
             * states
             */
            if (addStates) {
                ((Subject) watershed)
                        .addState(States.getView(drainageAreaState, watershed));
                ((Subject) watershed)
                        .addState(States.getView(streamNetworkState, watershed));
                ((Subject) watershed)
                        .addState(States.getView(flowDirectionsState, watershed));
                ((Subject) watershed).addState(States.getView(dem, watershed));
            }
            ret.add(watershed);
        }

        return ret;
    }
    
    public void createGridsInWatersheds() {
        this.createGridInInstances = true;
    }

}
