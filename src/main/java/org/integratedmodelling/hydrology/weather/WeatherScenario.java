package org.integratedmodelling.hydrology.weather;

import java.util.ArrayList;
import java.util.Date;

/**
 * Chainable changes in any weather variable that will affect weather generation when
 * observations are retrieved. 
 * 
 * @author Ferd
 *
 */
public class WeatherScenario {

    // modes of distribution of quantitative changes
    final static public int DISTRIBUTION_CONSTANT = 0;
    final static public int DISTRIBUTION_RANDOM   = 1;

    // modes of application of quantitative amount of change
    final static public int APPLICATION_ABSOLUTE     = 0;
    final static public int APPLICATION_PROPORTIONAL = 1;

    /*
     * Further changes to apply in sequence
     */
    ArrayList<WeatherScenario> _scenarios = new ArrayList<WeatherScenario>();

    int    _distribution;
    int    _application;
    double _amount;
    String _variable;
    Date   _from = null;
    Date   _to   = null;

    /**
     * A change across the board, either proportional or absolute. Can be
     * chained to other changes using add().
     * 
     * @param variable
     * @param amount
     * @param applicationMode 
     * @param distributionMode 
     */
    public WeatherScenario(String variable, double amount, int applicationMode, int distributionMode) {
        _variable = variable;
        _amount = amount;
        _application = applicationMode;
        _distribution = distributionMode;
    }

    /**
     * A proportional or absolute change in a variable that applies to a limited time span. Can be
     * chained to other changes using add().
     * 
     * @param variable
     * @param amount
     * @param from
     * @param to
     * @param applicationMode
     * @param distributionMode
     */
    public WeatherScenario(String variable, double amount, Date from, Date to, int applicationMode,
            int distributionMode) {
        _variable = variable;
        _amount = amount;
        _application = applicationMode;
        _distribution = distributionMode;
        _from = from;
        _to = to;
    }

    /**
     * Add a new scenario (usually in a different variable). Same parameters as the corresponding
     * constructor. Returns this so multiple changes can be stringed together.
     * 
     * @param variable
     * @param amount
     * @param applicationMode
     * @param distributionMode
     * @return a new scenario
     */
    public WeatherScenario add(String variable, double amount, int applicationMode, int distributionMode) {
        _scenarios.add(new WeatherScenario(variable, amount, applicationMode, distributionMode));
        return this;
    }

    /**
     * Add a new scenario (usually in a different variable). Same parameters as the corresponding
     * constructor. Returns this so multiple changes can be stringed together.
     *
     * @param variable
     * @param amount
     * @param from
     * @param to
     * @param applicationMode
     * @param distributionMode
     * @return a new scenario
     */
    public WeatherScenario add(String variable, double amount, Date from, Date to, int applicationMode, int distributionMode) {
        _scenarios.add(new WeatherScenario(variable, amount, from, to, applicationMode, distributionMode));
        return this;
    }
}
