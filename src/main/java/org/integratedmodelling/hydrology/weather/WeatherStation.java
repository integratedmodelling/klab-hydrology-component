/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.hydrology.weather;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.hydrology.weather.generator.WeatherGenerator;

import com.vividsolutions.jts.geom.Geometry;

public class WeatherStation implements IGeometricShape {

    public double                 longitude;
    public double                 latitude;
    public double                 elevation;
    public String                 id;
    public int                    lastKnownYear;
    public ShapeValue             location;

    /*
     * this will be non-null only after train() has been called. The station must
     * start with data anyway.
     */
    WeatherGenerator              wg      = null;

    // var -> local yearly data, set externally from data to use for matching to other
    // locations
    public Map<String, Double>    refData = new HashMap<>();

    /**
     * If we have initialized the station in a temporal context, this
     * will contain the data available in it, either for weather computation
     * or for creating the state of a weather station subject.
     */
    private Map<String, double[]> data    = new HashMap<>();

    public WeatherStation(Map<?, ?> rm) {
        for (Object o : rm.keySet()) {
            switch (o.toString()) {
            case "lon":
                longitude = ((Number) rm.get(o)).doubleValue();
                break;
            case "lat":
                latitude = ((Number) rm.get(o)).doubleValue();
                break;
            case "id":
                id = rm.get(o).toString();
                break;
            case "elevation":
                elevation = ((Number) rm.get(o)).doubleValue();
                break;
            default:
                if (rm.get(o) instanceof List) {
                    data.put(o.toString(), getDoubleData((List<?>) rm.get(o)));
                }
            }
        }

        this.location = new ShapeValue(longitude, latitude, Geospace.get().getDefaultCRS());

    }

    private double[] getDoubleData(List<?> data) {
        double[] ret = new double[data.size()];
        for (int i = 0; i < data.size(); i++) {
            ret[i] = data.get(i) instanceof Number ? ((Number) data.get(i)).doubleValue() : Double.NaN;
        }
        return ret;
    }

    @Override
    public Type getGeometryType() {
        return location.getGeometryType();
    }

    @Override
    public double getArea() {
        return location.getArea();
    }

    @Override
    public String asText() {
        return location.asText();
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return location.getGeometry();
    }

    @Override
    public Geometry getGeometry() {
        return location.getGeometry();
    }

    @Override
    public int getSRID() {
        return location.getSRID();
    }

    @Override
    public boolean isEmpty() {
        return location.isEmpty();
    }


    @Override
    public Class<?> getGeometryClass() {
        return getGeometry() == null ? null : getGeometry().getClass();
    }
    
    /**
     * Train a weather generator to represent the year range passed, and from this point
     * on use it to produce any numbers requested (changing the type to SIMULATED). When
     * calling this, we should be sure that there are no NaNs in the data for the years
     * (guaranteed with CRU data) and of course that the data for the training years are
     * available.
     * 
     * @param startYear
     * @param endYear
     * @throws KlabException 
     */
    public void train(int startYear, int endYear) throws KlabException {
        this.wg = new WeatherGenerator(this, -1, startYear, endYear);
    }

    public Map<String, double[]> getData() {
        return data;
    }

    /**
     * Use previous training to generate data for one year. Note that this will
     * only generate one year and leave whatever training data were in the data
     * buffer after that. So ensure that only the first year of data is accessed
     * after this is called.
     * 
     * @param year currently not used
     * @param variables unused, either - wg only generates prec, mint and maxt.
     */
    public void generateData(int year, String[] variables) {
        
        if (wg == null) {
            throw new KlabRuntimeException("cannot generate data in untrained weather station");
        }
        double[] rain = data.get(WeatherFactory.PRECIPITATION_MM);
        double[] minT = data.get(WeatherFactory.MIN_TEMPERATURE_C);
        double[] maxT = data.get(WeatherFactory.MAX_TEMPERATURE_C);
        
        if (rain == null) {
            rain = new double[366];
            data.put(WeatherFactory.PRECIPITATION_MM, rain);
        }

        if (minT == null) {
            minT = new double[366];
            data.put(WeatherFactory.MIN_TEMPERATURE_C, minT);
        }

        if (maxT == null) {
            maxT = new double[366];
            data.put(WeatherFactory.MAX_TEMPERATURE_C, maxT);
        }

        wg.generateDaily(minT, maxT, rain, false);
    }

}
