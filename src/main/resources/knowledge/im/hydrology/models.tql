namespace im.hydrology.models
	using im, earth, geography, hydrology, landcover;

assess measure geography:Aspect in degree_angle
   observing
      (measure geography:Elevation in m) named elevation	
   using gis.aspect();

assess measure geography:SlopeLength in m
   observing
      (measure geography:Elevation in m) named elevation
	using gis.slope-length();
     
/**
 * hydrological qualities: we compute and (normally) use all these together, so 
 * we create them all with one long-term process whose resultant is the full set 
 * of watershed-related qualities. 
 * 
 * The process model we link to will be set up so that if anything changes the 
 * independent variable (elevation), the states will turn dynamic and the
 * process will start rebuilding the observations.
 * 
 * The TCA threshold chosen here seems to pick most streams without
 * interruption. For the time being I'm hesitant adding a dependency on
 * actual stream subjects, but the underlying JGrassTools has the
 * ability of using them to "snap" the river network to passed
 * features, so that should happen at some point.
 * 
 */
model hydrology:WatershedFormation,
 	  //  pit-filled land elevation.
      (measure hydrology:Elevation in m) optional named elevation,
      // ranked for the purposes of the algorithms. Because FlowDirection is a physical property, this
      // model will not be used to resolve the concept (as Thinklab will be looking for a measurement).
      (rank hydrology:FlowDirection) optional named flow-direction,
      // total contributing area
      (measure hydrology:ContributingArea in m^2) optional named tca,
      // channel network
      (presence of earth:Stream) optional named stream-presence
   observing 
   	  (measure geography:Elevation in m) named elevation
   over space( grid = unknown )
   using im.hydrology.watershed-formation(tca-threshold = 0.005);

/**
 * The basic IM water model: surface runoff and groundwater supply vulnerability. Will
 * behave differently if an upstream model has computed both infiltration and evaporation.
 */
 model hydrology:SurfaceWaterFlow,
 		(measure hydrology:RunoffWaterVolume in mm/day) named runoff-amount,
 		// the next only gets created if we don't find infiltration and evaporation from upstream models
 		(measure hydrology:LocallyExchangedWaterVolume in mm/day) optional named vertical-transfer-amount,
   	    // runoff velocity
        (measure hydrology:RunoffVelocity in m/day) optional named runoff_velocity 	
   observing
 		(measure earth:PrecipitationVolume in mm) named precipitation,
      	(rank hydrology:FlowDirection) named flow-direction,
      	(presence of earth:Stream) named stream-presence,
      	(measure geography:Slope in degree_angle) named slope,
 		(classify hydrology:HydrologicSoilGroup) named hsg,
 		(classify landcover:GlobCoverType) named globcover
 	over space (grid = unknown)
 	using im.hydrology.surface-water-flow();

/**
 * Watershed instantiator. By default extracts partial watersheds
 * to cover the full region.
 */
model each hydrology:Watershed
   observing 
      (measure geography:Elevation in m) named elevation
      over space( grid = unknown )
   using im.hydrology.watersheds(tca-threshold = 0.005, whole-watersheds = false, minimum-basin-drainage = 0.01);

model presence of earth:Flood
   observing
      (measure geography:Slope in degree_angle) named slope,
      (measure hydrology:ContributingArea in km^2) named flowacc
   on definition set to [  
      def mti = (Math.log10((flowacc+1) * space.grid.cellArea) ** 0.08)/Math.tan(slope+0.001);
      def n = 0.016 * Math.pow(space.grid.cellArea, 0.46);
      def mtt = (10.89 * n) + 2.282;
      return mti > mtt;
    ]
        with metadata {
         dc:originator "Manfreda S., Di Leo M., Sole A., Detection of Flood Prone Areas using Digital Elevation Models, Journal of Hydrologic Engineering, (10.1061/(ASCE)HE.1943-5584.0000367), 2011."
         dc:url "https://grass.osgeo.org/grass70/manuals/addons/r.hazard.flood.html"
            im:reliability 10
            im:distribution "public"
        }
;

model each hydrology:Floodplain named floodplain-computed
   observing (presence of earth:Flood) named flooded
   using gis.extract-features(select = [flooded]);
