# k.LAB hydrology component
 
This component provides hydrological and weather models for usage within k.LAB. These services are all adaptive and will match the temporal and spatial scale of the context they run into. Spatial resolutions below 90m and temporal resolution below 1 day will be run with interpolation, but should be considered inaccurate with the available data. The specific services provided are listed below.

The simple surface runoff model computes surface water runoff in mm using the DEM and weather model as an input. Starting with a pit-filled digital elevation model, the model first delineates the corresponding watershed(s) in the context; for each watershed, the system computes flow directions, the flow accumulation and the river network. A routing algorithm uses landcover- and soil-derived curve numbers (using the best available data from the IM network) to compute the runoff fraction in each cell and run water across the watershed at each time step. Runoff velocity is also computed based on Manning's coefficient computed from land cover type.

If infiltration and evapotranspiration data are available in the context at the time the surface water flow process is observed, the model will automatically use them to compute the runoff fraction instead of relying on curve numbers.

These models run in a fully distributed fashion on gridded spatial contexts. They represent a preliminary strategy for water modeling in k.LAB, to be substituted with more modular, flexible and sophisticated approach representing more realms and configurations in the near future. 

## Authors
 
 * Ferdinando Villa <ferdinando.villa@bc3research.org>
 * Javier Martinez-Lopez <javier.martinez@bc3research.org>
 
## Services provided

### im.hydrology.surface-water-flow

Implements a simple hydrological surface transport model for the hydrology:SurfaceWaterFlow model. It only depends externally on geography:Elevation; other dependencies are provided within this component. It can run at any spatial and temporal scale as long as the spatial context is gridded.

### im.hydrology.watershed-formation

Contextualizes the earth:WatershedFormation process, performing watershed delineation and computing flow direction, contributing area and stream network.

### im.hydrology.watersheds

Uses the same model of im.hydrology.watershed-formation to instantiate watersheds as independent subjects. Will optionally provide them with a gridded spatial context if the grid=true option is passed.

### weather.get-stations

Instantiates weather station subjects with a point spatial context, interfacing with the service available at http://www.integratedmodelling.org/wmengine (see weather.get-weather for more).

### weather.get-weather

Computes weather records at arbitrary spatial and temporal resolution. Uses the IM weather engine at  http://www.integratedmodelling.org/wmengine, containing any weather station record uploaded by the community, all station records from NOAA's [Global Historic Climatology Network](https://www.ncdc.noaa.gov/data-access/land-based-station-data/land-based-datasets/global-historical-climatology-network-ghcn) and the East Anglia Univ. CRU dataset as a fallback. Station data are automatically interpolated and corrected for altitude to match the spatial extent and resolution of the context. A weather generator is optionally used to fill gaps in data.

## License

This software is licensed with the Affero General Public License v 3.0. See the main k.LAB distribution for more details.

